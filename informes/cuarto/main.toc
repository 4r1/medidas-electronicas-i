\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Materiales e Instrumental necesarios}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Introducci\IeC {\'o}n}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}C\IeC {\'a}lculos para la medici\IeC {\'o}n de la resistencia de salida de amplificadores con realimentaci\IeC {\'o}n.}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}Desarrollo}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Experimento 1: Determinaci\IeC {\'o}n de la Resistencia de salida a lazo abierto y a lazo cerrado.}{5}{section.2.1}
\contentsline {subsection}{Paso 1}{6}{section*.2}
\contentsline {subsection}{Paso 2}{6}{section*.3}
\contentsline {subsection}{Paso 3}{6}{section*.4}
\contentsline {subsection}{Paso 4}{7}{section*.5}
\contentsline {section}{\numberline {2.2}Experimento 2: Medici\IeC {\'o}n de la potencia eficaz m\IeC {\'a}xima de salida.}{7}{section.2.2}
\contentsline {subsection}{Paso 5}{7}{section*.6}
\contentsline {section}{\numberline {2.3}Experimento 3: Ensayo de la respuesta en frecuencia (ancho de banda) del amplificador}{8}{section.2.3}
\contentsline {subsection}{Paso 6}{8}{section*.7}
\contentsline {section}{\numberline {2.4}Experimento 4: Determinaci\IeC {\'o}n de la respuesta en frecuencia del amplificador funcionando a lazo abierto mediante el empleo de una onda cuadrada}{10}{section.2.4}
\contentsline {subsection}{Paso 7}{10}{section*.8}
\contentsline {chapter}{\numberline {3}Conclusion}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Enunciado}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Conclusiones}{13}{section.3.2}
