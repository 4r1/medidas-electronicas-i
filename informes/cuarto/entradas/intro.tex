\chapter{Introducción}
\section{Objetivo}
Determinar características (ganancia e impedancias de entrada y salida) de un amplificador con realimentación negativa, en condiciones de lazo abierto y cerrado; poniendo en evidencia las ventajas y desventajas de la realimentación negativa.

\section{Materiales e Instrumental necesarios}
\begin{itemize}
  \item Amplificador transistorizado de dos etapas con realimentación.
  \item Fuente de alimentación +12V.
  \item Generador de baja frecuencias con salida senoidal y cuadrada.
  \item Osciloscopio de usos generales.
\end{itemize}

\section{Introducción}
En esta actividad práctica, se emplearán los mismos métodos e instrumentos utilizados en el trabajo práctico “Instrumentos con escalas en dB”, pero ahora para determinar las características de ganancia e impedancias de salida y entrada de un amplificador, que posee un sistema de realimentación negativa, ensayado en condiciones de lazo abierto y de lazo cerrado. 

Se pretende, por un lado poner en evidencia las ventajas y/o desventajas del empleo de la realimentación negativa, y por el otro comprobar si los métodos de medición ya experimentados son igualmente útiles en esta situación, y en caso de haber alguna variante, tomar nota de la misma.

En principio, la medida de la resistencia de salida de un amplificador puede efectuarse mediante un método que consiste en medir primero la tensión de salida a circuito abierto (sin carga), y a continuación volver a medir, pero esta vez con una carga de valor conocido.
La resistencia de salida (Ro) puede obtenerse deduciendo su valor mediante un simple cálculo y empleando la ley de Ohm.  

\begin{center}\includegraphics{../imagenes/imag1.png}\end{center}

En la práctica lo que se suele hacer es utilizar una carga  variable (Rl), que se ajusta hasta que la tensión de salida se reduzca a la mitad de la que se obtiene con la carga desconectada.
 En esta situación Ro es igual a Rl.

El método es bastante simple, y no se presentan mayormente problemas, en tanto no haya componentes reactivas apreciables (en realidad sería mas apropiado hablar de Impedancia de salida en lugar de resistencia de salida), y siempre y cuando el amplificador no esté realimentado.

En cambio, si el amplificador tiene un sistema de realimentación negativa, puede haber dificultades porque uno de los efectos de la realimentación es mantener estable la amplitud de la salida del amplificador aunque varíe la carga conectada, lo cual implica una reducción de la resistencia de salida.

Si se intenta variar la carga hasta que la tensión de salida se reduzca a la mitad de la que hay sin carga, puede ocurrir que se lleve el punto de funcionamiento del amplificador a una zona donde la potencia disipada exceda los limites previstos y se dañe. 

\section{Cálculos para la medición de la resistencia de salida de amplificadores con realimentación.}
Se estudiará a continuación, el caso de la medición de la resistencia de salida de un amplificador lineal que tiene un sistema de realimentación negativa de tensión en serie.
Se definen los siguientes  parámetros:
\begin{description}
  \item[A]Ganancia de lazo abierto del amplificador.
  \item[B]Ganancia de lazo de realimentación.
  \item[$R_o$]Resistencia de salida de lazo abierto.
  \item[$R_o'$]Resistencia de salida de lazo cerrado.
  \item[G]Ganancia del amplificador a lazo cerrado (con realimentación).
\end{description}

Considérese el siguiente circuito equivalente:

\begin{center}\includegraphics{../imagenes/imag2.png}\end{center}


La medida de la resistencia interna de un circuito, puede plantearse, al menos teóricamente, de la siguiente manera: Se aplica una tensión al circuito y se mide la corriente que circula.

La resistencia interna es; por ley de Ohm;  simplemente la relación entre la tensión  y la corriente.

En el circuito equivalente mostrado, Vo es una  tensión hipotética que se aplica  en tanto que Io es la corriente (también hipotética) que circula.

Entonces del circuito puede escribirse que:
\\~\\

\begin{tabu}{rclcrcl}
  $V_o $ & = & $ A \cdot V_i' + R_o \cdot I_o                     $ & $ \therefore$ & $V_o           $              & = & $ A \left[ V_i - \left( B \cdot V_o\right) \right] + R_o \cdot I_o $\\
  $V_o $ & = & $ A \cdot V_i - A \cdot B \cdot V_o + R_o \cdot I_o$ & ;             & $ V_o + V_o \cdot A \cdot B $ & = & $ A \cdot V_i + R_o \cdot I_o$ \\
\end{tabu}
\\~\\
$$V_o \cdot \left( 1 + A \cdot B \right) = A \cdot V_i + R_o \cdot I_o$$

$$V_o = \dfrac{A}{1 + A \cdot B} \cdot V_i + \dfrac{R_o}{1 + A \cdot B} \cdot I_o$$
\\
\begin{align*}
  G &= \dfrac{A}{1 + A \cdot B} \\
  \Aboxed{1 + A \cdot B &= \dfrac{A}{G}}
\end{align*}

\begin{align*}
  R_o' &= \dfrac{R_o}{1 + A \cdot B} \\
  \Aboxed{R_o' &= \dfrac{R_o \cdot G}{A}}
\end{align*}

En otras palabras, para determinar la resistencia de salida del amplificador realimentado (Ro´), se deben medir por separado: la resistencia de salida a lazo abierto (Ro) - que se puede hacer por el método descrito inicialmente -  la  ganancia a lazo abierto (A), y la ganancia a lazo cerrado (G),  - o bien la ganancia del lazo (B) -.

El valor definitivo se obtiene por cálculo empleando las ecuaciones  I , II  o III, (la que resulte más fácil en la práctica).
