\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Materiales e instrumental necesarios}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Introducci\IeC {\'o}n}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}Desarrollo}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Estimaci\IeC {\'o}n de valores}{3}{section.2.1}
\contentsline {subsection}{Consigna}{3}{section*.2}
\contentsline {subsection}{Resoluci\IeC {\'o}n}{3}{section*.3}
\contentsline {subsubsection}{Circuito equivalente de alterna reflejado a la base}{3}{section*.4}
\contentsline {paragraph}{C\IeC {\'a}lculo de impedancia de entrada del transistor en configuraci\IeC {\'o}n emisor com\IeC {\'u}n con la salida en cortocircuito ($hie$)}{3}{section*.5}
\contentsline {subsubsection}{Impedancia de entrada}{4}{section*.6}
\contentsline {subsubsection}{Impedancia de salida}{4}{section*.7}
\contentsline {subsubsection}{Ganancia de potencia}{5}{section*.8}
\contentsline {section}{\numberline {2.2}Experimento 1: Determinaci\IeC {\'o}n de la impedancia de salida del amplificador}{5}{section.2.2}
\contentsline {subsection}{Consigna}{5}{section*.9}
\contentsline {subsection}{Resoluci\IeC {\'o}n}{6}{section*.11}
\contentsline {section}{\numberline {2.3}Experimento 2: Determinaci\IeC {\'o}n de la impedancia de entrada del amplificador}{6}{section.2.3}
\contentsline {subsection}{Consigna}{6}{section*.13}
\contentsline {subsection}{Resoluci\IeC {\'o}n}{7}{section*.15}
\contentsline {section}{\numberline {2.4}Experiencia 3: Medici\IeC {\'o}n de la ganancia del amplificador}{7}{section.2.4}
\contentsline {subsection}{Consigna}{7}{section*.17}
\contentsline {subsection}{Resoluci\IeC {\'o}n}{8}{section*.19}
\contentsline {section}{\numberline {2.5}Experiencia 4: Medici\IeC {\'o}n de la potencia de salida del amplificador}{9}{section.2.5}
\contentsline {subsection}{Consigna}{9}{section*.21}
\contentsline {subsection}{Resoluci\IeC {\'o}n}{9}{section*.22}
\contentsline {chapter}{\numberline {3}Conclusi\IeC {\'o}n}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Enunciado}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Conclusiones}{10}{section.3.2}
