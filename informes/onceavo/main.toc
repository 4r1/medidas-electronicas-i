\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Material e instrumental necesarios}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Introducci\IeC {\'o}n}{1}{section.1.3}
\contentsline {chapter}{\numberline {2}Desarrollo}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Experimento 1: Analisis de una forma de onda cuadrada.}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Experimento 2: An\IeC {\'a}lisis de un tren de pulsos}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Experimento 3: Observacion de frecuencias ``Alias''}{8}{section.2.3}
\contentsline {section}{\numberline {2.4}Experimento 4: An\IeC {\'a}lisis de una se\IeC {\~n}al modulada en amplitud}{8}{section.2.4}
\contentsline {section}{\numberline {2.5}Experimento 5: Observaci\IeC {\'o}n de los productos de IMD de tercer orden}{10}{section.2.5}
\contentsline {section}{\numberline {2.6}Experimento 6: An\IeC {\'a}lisis de una se\IeC {\~n}al modulada en frecuencia}{11}{section.2.6}
\contentsline {section}{\numberline {2.7}Experimento 7: An\IeC {\'a}lisis de la distorsi\IeC {\'o}n arm\IeC {\'o}nica producida por un amplificador}{13}{section.2.7}
\contentsline {chapter}{\numberline {3}Conclusi\IeC {\'o}n}{16}{chapter.3}
\contentsline {section}{\numberline {3.1}Conclusiones finales}{16}{section.3.1}
