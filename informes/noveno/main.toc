\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Materiales e instrumental necesarios}{1}{section.1.2}
\contentsline {chapter}{\numberline {2}Desarrollo}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Experimento 1: Determinaci\IeC {\'o}n de la velocidad de propagaci\IeC {\'o}n en una l\IeC {\'\i }nea de transmisi\IeC {\'o}n mediante m\IeC {\'e}todo reflectom\IeC {\'e}trico.}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Experimento 2: Determinaci\IeC {\'o}n de la impedancia caracter\IeC {\'\i }stica una l\IeC {\'\i }nea de transmisi\IeC {\'o}n mediante m\IeC {\'e}todo reflectom\IeC {\'e}trico.}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Experimento 3: Medici\IeC {\'o}n de la atenuaci\IeC {\'o}n por unidad de longitud.}{6}{section.2.3}
\contentsline {chapter}{\numberline {3}Conclusi\IeC {\'o}n}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Consignas}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Conclusiones}{9}{section.3.2}
