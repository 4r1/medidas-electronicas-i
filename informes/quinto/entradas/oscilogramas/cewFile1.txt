Analog Ch  State   Scale    Position   Coupling  BW Limit  Invert
CH1        On      200mV/   8.00mV     AC        Off       Off
CH2        On      20.0V/    800mV     AC        Off       Off

Analog Ch  Impedance   Probe
CH1        1M Ohm      10X
CH2        1M Ohm      10X

Time    Time Ref    Main Scale    Delay
Main    Center      10.00ms/      0.000000s

Trigger  Source      Slope    Mode      Coupling     Level    Holdoff
Edge     CH1         Rising   Auto      DC           0.00uV   500ns

Acquisition    Sampling    Memory Depth    Sample Rate
Normal         Realtime    Normal          25.00kSa    

